"use strict";

var SETTINGS			= require("./conf/settings");

process.env.SERVERNAME	 = SETTINGS.CONF.serverName	            || "Google API test";
process.env.NODE_ENV	   = SETTINGS.CONF.envMode		            || "development";
process.env.HOST		     = process.env.OPENSHIFT_NODEJS_IP      || SETTINGS.CONF.host || "localhost";
process.env.PORT		     = process.env.OPENSHIFT_NODEJS_PORT    || SETTINGS.CONF.port || 3500;

process.env.DB_HOST      = process.env.OPENSHIFT_MONGODB_DB_HOST            || "localhost";
process.env.DB_PORT      = process.env.OPENSHIFT_MONGODB_DB_PORT            || 27017;
process.env.DB_NAME      = SETTINGS.DB.name         	          || "mapapp";
process.env.DB_URL       = process.env.OPENSHIFT_MONGODB_DB_URL + '/' + process.env.DB_NAME ||
                           process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME;

var express           = require('express');
var path              = require('path');
var favicon           = require('serve-favicon');
var logger            = require('morgan');
var cookieParser      = require('cookie-parser');
var bodyParser        = require('body-parser');
var expressSession    = require('express-session');
var hbs               = require('express-handlebars');
var mongoose          = require('mongoose');

var app = express();

mongoose.connect(process.env.DB_URL, function(error) {
  if (error) {
    console.log("MongoDB unavailable");
  } else {
    console.log("Connected to MongoDB");
  }
});

// view engine setup
app.engine('hbs', hbs({ extname: 'hbs', defaultLayout: "layout", layoutsDir: __dirname + '/views/layouts' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(express.static(path.join(__dirname, '/public')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(expressSession({secret: '^#brunexilibonátor$', saveUninitialized: true, resave: true}));

require("./routes/_routing")(express, app);

// enable CORS (cross-origin resource sharing)
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
