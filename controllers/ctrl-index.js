"use strict";

var ctrl = {
	"show" : function(req, res, next) {
		res.status(200).render("index", { "title" : "Home" });
	}	
}

Object.keys(ctrl).forEach(function(key) {
	exports[key] = ctrl[key];
});