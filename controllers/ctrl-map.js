"use strict";

var Path = require("../models/model-path").Path;

var ctrl = {
	"get" : function(req, res, next) {
        Path.find(function(error, docs) {
            if (error) {
                res.send({ "success" : false, "error" : error });
            } else {
                if (docs) {
                    res.send({ "success" : true, "paths" : docs });
                } else {
                    res.send({ "success" : false, "error" : "No entry in database" });
                }
            }
        })
	}
,   "add" : function(req, res, next) {
        var data = req.body;
        var path = Path(data);

        //console.log(data);

        path.save(function(error, doc, n) {
            if (error) {
                console.log("Error: " + error);
                res.send({ "success" : false, "error" : error });
            } else {
                console.log("Data inserted");
                res.send({ "success" : true });
            }
        });
    }
}

Object.keys(ctrl).forEach(function(key) {
	exports[key] = ctrl[key];
});