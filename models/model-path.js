"use strict;"

var mongoose = require('mongoose');

var pathSchema = new mongoose.Schema({
    path    : { type: Array, required: true }
}, {collection: "Paths", strict: false });

var Path = mongoose.model('Path', pathSchema);

exports.Path = Path;