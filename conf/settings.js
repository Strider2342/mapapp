exports.CONF = {
	serverName  : "Google API test",
    host		: "localhost",
	port		: 4500,
    envMode		: "development"		// or production
}

exports.DB = {
    host        : "localhost",
    port        : 27017,
    name        : "mapapp"
}