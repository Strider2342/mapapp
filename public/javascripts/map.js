var map, poly, line;
var getbutton, postbutton;
var pathsData;

function initMap() {
    getbutton = document.getElementById('getbtn');

    // the div that will contain the map
    var mapDiv = document.getElementById('map');
    // the map object
    map = new google.maps.Map(mapDiv, {
        center: { lat: 47.090, lng: 17.906 },
        zoom: 16
    });

    polyLine = new google.maps.Polyline({
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2,
        map: map
    });

    coordinates = [];

    poly = new google.maps.Polygon({
        paths: coordinates,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        editable: true,
        map: map
    });
}

function getBounds() {
    window.ajax.get('/map', function(data) {
        pathsData = data;
        
        if (JSON.parse(pathsData).paths.length > 1) {
            console.log("Boundaries loaded!");

            drawPoly(0);       
        } else {
            console.log("No entry in database!");
        }    
    })
}

function postBounds() {
    if (poly.getPaths().b[0] !== undefined) {
        var paths = poly.getPaths().b[0].b;        
        var pathArray = [];

        paths.forEach(function(entry) {
            pathArray.push({ lat: entry.lat(), lng: entry.lng() });
        });

        data = {
            name: document.getElementById("currentname").innerHTML,
            path: pathArray
        }
        
        window.ajax.post(data, '/map', function(data) {
            // callback
        });        
    } else {
        console.log("Boundaries not yet loaded!");
    }
}

function choose() {
    var current = 0;
    var radios = document.getElementsByName("pathradio");

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            current = i;
        }
    }

    drawPoly(current);
}

function drawPoly(current) {
    path_json = JSON.parse(pathsData).paths[current].path;
    coordinates = [];

    path_json.forEach(function(entry) {
        coordinates.push({ lat: entry.lat, lng: entry.lng });
    });

    poly.setOptions({ paths: coordinates });
    showInfo(current);
}

function showInfo(current) {
    document.getElementById("choser").innerHTML = "";
    for (var i = 0; i < JSON.parse(pathsData).paths.length; i++) {
        document.getElementById("choser").innerHTML += "<input name='pathradio' type='radio' value='" + (i + 1) + "' onclick='drawPoly(" + i + ")'>" + (i + 1) + "</input><br>";
    }
    document.getElementById("choser").innerHTML += "<button onclick='choose()'>Change</button>";

    document.getElementById("numofdocs").innerHTML = JSON.parse(pathsData).paths.length;
    document.getElementById("currentname").innerHTML = JSON.parse(pathsData).paths[current].name;
    document.getElementById("currentpaths").innerHTML = JSON.stringify(JSON.parse(pathsData).paths[current].path);
}

function toggleEdit() {
    poly.setOptions({ editable: !(poly.getEditable()) });
}